using FluentAssertions;

namespace Tully.CodeTest.ActivTrak.UnitTests;

public class TestRegionFinder
{
    /// <summary>The test grid provided by the problem</summary>
    /// <remarks>NB: This is visually inverted, but the indices match the provided graphic semantically</remarks>
    private readonly int[,] _canonicalTestGrid =
    {
        {   0,  80,  45,  95, 170, 145 },
        { 115, 210,  60,   5, 230, 220 },
        {   5,   0, 145, 250, 245, 140 },
        {  15,   5, 175, 250, 185, 160 },
        {   0,   5,  95, 115, 165, 250 },
        {   5,   0,  25,   5, 145, 250 },
    };

    [Fact]
    public void CanonicalExample_ReturnsThreeRegions()
    {
        var sut = new RegionFinder(_canonicalTestGrid, 200);
        var result = sut.Detect().ToList();
            
        result.Should().HaveCount(3, "because there are three lights");
        
        result.Should().AllSatisfy(subregion => subregion.CoordinatePairs.Should().NotBeEmpty());
        
        result.Should().AllSatisfy(subregion =>
            subregion.CoordinatePairs.Should().AllSatisfy(loc =>
                _canonicalTestGrid[loc.Y, loc.X].Should().BeGreaterOrEqualTo(200)
            )
        );
    }

    /// <summary>
    /// Include all cases which could potentially trip up the algorithm
    /// </summary>
    /// <remarks>
    /// 999: axially aligned adjacency as a block
    /// 888: axially aligned adjacency as a hollow square
    /// 777: diagonally aligned adjacency as a hollow diamond
    /// 111: requires stepping in every direction from origin
    ///  55: a single shape connecting all nonzero subregions
    /// </remarks>
    private readonly int[,] _exhaustiveTestGrid =
    {
        {   0,   0,   0,   0,   0, 777,   0 },
        {   0, 999, 999,  55, 777,   0, 777 },
        {   0, 999, 999,   0,   0, 777,   0 },
        {   0,   0,   0,  55,   0,   0,   0 },
        {   0, 888, 888, 888,   0,   0,   0 },
        {   0, 888,   0, 888,   0,   0,   0 },
        {   0, 888, 888, 888,   0,   0,   0 },
        {   0,   0,   0,   0,  55,   0,   0 },
        {   0,   0,   0, 111,   0,   0, 111 },
        { 111,   0,   0,   0, 111,   0, 111 },
        { 111,   0,   0, 111,   0, 111,   0 },
        {   0, 111, 111, 111,   0, 111, 111 },
    };

    [Fact]
    public void ExhaustiveExample_FindsSolidSquare()
    {
        var sut = new RegionFinder(_exhaustiveTestGrid, 900);
        var result = sut.Detect().ToList();

        var solidSquare = result.Should().ContainSingle().Subject; 
        solidSquare.CoordinatePairs.Should().HaveCount(4)
            .And.Contain([(1, 1), (1, 2), (2, 1), (2, 2)]);
        solidSquare.CenterOfMass.X.Should().Be(1.5m);
        solidSquare.CenterOfMass.Y.Should().Be(1.5m);
    }

    [Fact]
    public void ExhaustiveExample_FindsHollowSquare()
    {
        var sut = new RegionFinder(_exhaustiveTestGrid, 800);
        var result = sut.Detect().ToList();

        result.Should().HaveCount(2);

        var hollowSquare = result.Should().ContainSingle(sub =>
                sub.CoordinatePairs.Any(loc =>
                    _exhaustiveTestGrid[loc.Y, loc.X] == 888))
            .Subject;

        hollowSquare.CoordinatePairs.Should().HaveCount(8);
        hollowSquare.CenterOfMass.X.Should().Be(2);
        hollowSquare.CenterOfMass.Y.Should().Be(5);
    }

    [Fact]
    public void ExhaustiveExample_FindsHollowDiamond()
    {
        var sut = new RegionFinder(_exhaustiveTestGrid, 700);
        var result = sut.Detect().ToList();

        result.Should().HaveCount(3);

        var hollowDiamond = result.Should().ContainSingle(sub =>
                sub.CoordinatePairs.Any(loc =>
                    _exhaustiveTestGrid[loc.Y, loc.X] == 777))
            .Subject;

        hollowDiamond.CoordinatePairs.Should().HaveCount(4);
        hollowDiamond.CenterOfMass.X.Should().Be(5);
        hollowDiamond.CenterOfMass.Y.Should().Be(1);
    }

    [Fact]
    public void ExhaustiveExample_FindsEveryDirectionShape()
    {
        var sut = new RegionFinder(_exhaustiveTestGrid, 100);
        var result = sut.Detect().ToList();

        result.Should().HaveCount(4);

        var hollowDiamond = result.Should().ContainSingle(sub =>
                sub.CoordinatePairs.Any(loc =>
                    _exhaustiveTestGrid[loc.Y, loc.X] == 111))
            .Subject;

        hollowDiamond.CoordinatePairs.Should().HaveCount(13);
    }

    [Fact]
    public void ExhaustiveExample_FindsSingleInterconnectedShape()
    {
        
        var sut = new RegionFinder(_exhaustiveTestGrid, 50);
        var result = sut.Detect().ToList();

        var singleShape = result.Should().ContainSingle().Subject;

        singleShape.CoordinatePairs.Should().HaveCount(32);
    }
    
    private readonly int[,] _centerOfMassTestGrid =
    {
        {   0,   0,   0,   0,   0,   0,   0 },
        {   0,   0,   0,   0,   0,   0,   0 },
        {   0,   0,   0,   0,   0,   4,   0 },
        {   0,  50,   1, 100, 101,   0,   0 },
        {   0,   4,   0,   0,   0,   0,   0 },
        {   0,   0,   0,   0,   0,   0,   0 },
        {   0,   0,   0,   0,   0,   0,   0 },
    };

    [Fact]
    public void CenterOfMass_FindsContrivedExample()
    {
        var sut = new RegionFinder(_centerOfMassTestGrid, 1);
        var result = sut.Detect().ToList();
        
        var singleShape = result.Should().ContainSingle().Subject;

        singleShape.CenterOfMass.X.Should().Be(3m);
        singleShape.CenterOfMass.Y.Should().Be(3m);
    }
}