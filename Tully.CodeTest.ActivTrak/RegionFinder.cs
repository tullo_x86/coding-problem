﻿using System.Numerics;
using System.Runtime.CompilerServices;
using Vector2Dec = (decimal X, decimal Y);
using Vector2Int = (int X, int Y);

namespace Tully.CodeTest.ActivTrak;

/// <summary>
/// Finds subregions within a signal grid.
/// </summary>
public class RegionFinder
{
    private readonly int[,] _valueGrid;
    private readonly bool[,] _visited;
    private readonly int _threshold;

    /// <param name="valueGrid">Signal sources.</param>
    /// <param name="threshold">Detection threshold. Inclusive comparison.</param>
    public RegionFinder(int[,] valueGrid, int threshold)
    {
        _valueGrid = valueGrid;
        _visited = new bool[valueGrid.GetLength(0), valueGrid.GetLength(1)];
        _threshold = threshold;
    }

    /// <summary>
    /// Detects subregions of contiguous signal values which match or exceed the given value.
    /// The subregion detection is effectively a flood-fill beginning at some point.
    /// </summary>
    /// <returns>A sequence of detected subregions above the signal threshold.</returns>
    /// <remarks>
    /// ASSUMPTION: Detection threshold is inclusive (subregion matches if greater-or-equal)
    /// ASSUMPTION: Threshold comparisons are cheap or somehow precalculated
    /// </remarks>
    public IEnumerable<Subregion> Detect()
    {
        // Strategy:
        //  (A) Find first match by searching left-to-right, top-to-bottom
        //      (a) once a match is found, no matching signal exists above
        //      (b) once a match is found, no matching signal exists in current row to the left
        //  (B) Flood-fill from this match
        //      (a) Using modified recursion-type flood fill from [1]
        // Implications of (A):
        //  Only (a) makes for a meaningful optimisation (unless threshold comparison is VERY expensive) 
        // 
        // [1] https://en.wikipedia.org/wiki/Flood_fill#Moving_the_recursion_into_a_data_structure
        
        // NB:  There is almost certainly some way to construct a heap that can represent this X-then-Y ordering so that
        //      we could do quick lookups while not needing to track the visited cells separately. If this algorithm
        //      needs to be scaled significantly, look into this.
        
        // Linear-search to find search origins
        for (int y = 0; y <= _valueGrid.GetUpperBound(0); y++)
        {
            for (int x = 0; x <= _valueGrid.GetUpperBound(1); x++)
            {
                if (!_visited[y, x] && _valueGrid[y, x] >= _threshold)
                {
                    yield return FloodFill(x, y);
                }
                else
                {
                    _visited[y, x] = true;
                }
            }
        }
    }

    private Subregion FloodFill(int initialX, int initialY)
    {
        List<Vector2Int> filledCoordinates = new();
        Vector2Dec centerOfMassAccumulator = (0m, 0m);
        decimal totalMass = 0m;
        Queue<Vector2Int> searchSpace = new();
        
        // https://en.wikipedia.org/wiki/Flood_fill#Moving_the_recursion_into_a_data_structure
        
        searchSpace.Enqueue((initialX, initialY));
        while (searchSpace.TryDequeue(out var location))
        {
            var value = _valueGrid[location.Y, location.X];
            if (!_visited[location.Y, location.X] && value >= _threshold)
            {
                filledCoordinates.Add(location);
                centerOfMassAccumulator.X += value * location.X;
                centerOfMassAccumulator.Y += value * location.Y;
                totalMass += value; 

                EnqueueIfNotVisited(searchSpace, location.X + 1, location.Y);       // E
                EnqueueIfNotVisited(searchSpace, location.X + 1, location.Y + 1);   // SE
                EnqueueIfNotVisited(searchSpace, location.X, location.Y + 1);       // S
                EnqueueIfNotVisited(searchSpace, location.X - 1, location.Y + 1);   // SW
                EnqueueIfNotVisited(searchSpace, location.X - 1, location.Y);       // W
                EnqueueIfNotVisited(searchSpace, location.X - 1, location.Y - 1);   // NW
                EnqueueIfNotVisited(searchSpace, location.X, location.Y - 1);       // N
                EnqueueIfNotVisited(searchSpace, location.X + 1, location.Y - 1);   // NE
            }
            _visited[location.Y, location.X] = true;
        }

        return new Subregion(
            filledCoordinates,
            (centerOfMassAccumulator.X / totalMass, centerOfMassAccumulator.Y / totalMass)
        );
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void EnqueueIfNotVisited(Queue<Vector2Int> searchSpace, int x, int y)
    {
        if (y >= 0 && y < _visited.GetLength(0) &&
            x >= 0 && x < _visited.GetLength(1) &&
            !_visited[y, x])
        {
            searchSpace.Enqueue((x, y));
        }
    }

    public record Subregion(IEnumerable<Vector2Int> CoordinatePairs, Vector2Dec CenterOfMass);
}
